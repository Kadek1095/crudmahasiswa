<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title> Mahasiswa</title>
</head>

<body>
    <section class="vh-100 d-flex">
        <div class="col-10 p-5">
            <h3>CRUD Mahasiswa</h3>
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#tambah"
                data-bs-whatever="@getbootstrap">Tambah</button>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Prodi</th>
                        <th scope="col">Fakultas</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($mahasiswa) > 0)
                        @foreach ($mahasiswa as $i => $mhs)
                            <tr>
                                <th>{{ $i + 1 }}</th>
                                <td>{{ $mhs->nama_mahasiswa }}</td>
                                <td>{{ $mhs->nim_mahasiswa }}</td>
                                <td>{{ $mhs->kelas_mahasiswa }}</td>
                                <td>{{ $mhs->prodi_mahasiswa }}</td>
                                <td>{{ $mhs->fakultas_mahasiswa }}</td>
                                <td>
                                    <div class="d-flex justify-content-around">
                                        <button type="button" class="btn btn-warning btn-sm mr-5" data-bs-toggle="modal"
                                            data-bs-target="#edit{{ $mhs->id }}"
                                            data-bs-whatever="@getbootstrap">Edit</button>
                                        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal"
                                            data-bs-target="#hapus{{ $mhs->id }}"
                                            data-bs-whatever="@getbootstrap">Hapus</button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <!-- edit modal -->
            @foreach ($mahasiswa as $mhs)
                <div class="modal fade" id="edit{{ $mhs->id }}" data-bs-backdrop="static"
                    data-bs-keyboard="false" tabindex="-1" aria-labelledby="editLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editLabel">Tambah Mahasiswa</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/edit/{{ $mhs->id }}">
                                    @csrf
                                    <div class="mb-2">
                                        <label for="nama" class="col-form-label">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama"
                                            value="{{ $mhs->nama_mahasiswa }}" required>
                                    </div>
                                    <div class="mb-2">
                                        <label for="nim" class="col-form-label">NIM</label>
                                        <input type="text" class="form-control" id="nim" name="nim"
                                            value="{{ $mhs->nim_mahasiswa }}" required>
                                    </div>
                                    <div class="mb-2">
                                        <label for="kelas" class="col-form-label">Kelas</label>
                                        <input type="text" class="form-control" id="kelas" name="kelas"
                                            value="{{ $mhs->kelas_mahasiswa }}" required>
                                    </div>
                                    <div class="mb-2">
                                        <label for="prodi" class="col-form-label">Prodi</label>
                                        <input type="text" class="form-control" id="prodi" name="prodi"
                                            value="{{ $mhs->prodi_mahasiswa }}" required>
                                    </div>
                                    <div class="mb-2">
                                        <label for="fakultas" class="col-form-label">Fakultas</label>
                                        <input type="text" class="form-control" id="fakultas" name="fakultas"
                                            value="{{ $mhs->fakultas_mahasiswa }}" required>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- tambah modal -->
            <div class="modal fade" id="tambah" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="tambahLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="tambahLabel">Tambah Mahasiswa</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/tambah">
                                @csrf
                                <div class="mb-2">
                                    <label for="nama" class="col-form-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" required>
                                </div>
                                <div class="mb-2">
                                    <label for="nim" class="col-form-label">NIM</label>
                                    <input type="text" class="form-control" id="nim" name="nim" required>
                                </div>
                                <div class="mb-2">
                                    <label for="kelas" class="col-form-label">Kelas</label>
                                    <input type="text" class="form-control" id="kelas" name="kelas" required>
                                </div>
                                <div class="mb-2">
                                    <label for="prodi" class="col-form-label">Prodi</label>
                                    <input type="text" class="form-control" id="prodi" name="prodi" required>
                                </div>
                                <div class="mb-2">
                                    <label for="fakultas" class="col-form-label">Fakultas</label>
                                    <input type="text" class="form-control" id="fakultas" name="fakultas" required>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- delete modal modal -->
            @foreach ($mahasiswa as $mhs)
                <div class="modal fade" id="hapus{{ $mhs->id }}" data-bs-backdrop="static"
                    data-bs-keyboard="false" tabindex="-1" aria-labelledby="hapus" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">Hapus Mahasiswa</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <p>menghapus data?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                <a href="/hapus/{{ $mhs->id }}" class="btn btn-danger">Hapus</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>

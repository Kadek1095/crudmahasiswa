<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    private $mhs_table;

    public function __construct(){
        $this->mhs_table = DB::table('mahasiswa');
    }

    public function index(){
        $data['mahasiswa'] = $this->mhs_table->get();
        return view('index', $data);
    }
    public function tambah(Request $request){
        $nama = $request->input('nama');
        $nim = $request->input('nim');
        $kelas = $request->input('kelas');
        $prodi = $request->input('prodi');
        $fakultas = $request->input('fakultas');

        $this->mhs_table->insert([
            'nama_mahasiswa' => $nama,
            'nim_mahasiswa' => $nim,
            'kelas_mahasiswa' => $kelas,
            'prodi_mahasiswa' => $prodi,
            'fakultas_mahasiswa' => $fakultas,
        ]);
        return redirect()->back();
    }
    public function edit(Request $request, $id){
        $nama = $request->input('nama');
        $nim = $request->input('nim');
        $kelas = $request->input('kelas');
        $prodi = $request->input('prodi');
        $fakultas = $request->input('fakultas');

        $this->mhs_table->where('id', $id)->update([
            'nama_mahasiswa' => $nama,
            'nim_mahasiswa' => $nim,
            'kelas_mahasiswa' => $kelas,
            'prodi_mahasiswa' => $prodi,
            'fakultas_mahasiswa' => $fakultas,
        ]);

        return redirect()->back();
    }
    public function hapus($id){
        $this->mhs_table->where('id', $id)->delete();
        return redirect()->back();
    }
}

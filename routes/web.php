<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\MahasiswaController::class, 'index']);
Route::post('/edit/{id}', [App\Http\Controllers\MahasiswaController::class, 'edit']);
Route::post('/tambah', [App\Http\Controllers\MahasiswaController::class, 'tambah']);
Route::get('/hapus/{id}', [App\Http\Controllers\MahasiswaController::class, 'hapus']);
